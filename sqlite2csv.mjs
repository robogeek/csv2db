
import stringify from 'csv-stringify';
import sqlite3 from 'sqlite3';

const dbfn    = process.argv[2];
const dbquery = process.argv[3];

const db = new sqlite3.Database(dbfn);
db.on('error', err => { 
    console.log(err);
    process.exit(-1);
});
// db.on('trace', sql => {
//     console.log(sql);
// });

let stringifier;
let columns;

await new Promise((resolve, reject) => {
    db.each(dbquery, function(err, row) {
        // This function is executed for each row in the result set.
        // For the first invocation, we initialize the columns and stringifier.
        // For every invocation, we write the row object to the stringifier.
        // The row object is a simple object where the fields match the columns
        // specified in the query.
        if (err) { reject(err); }
        if (!columns) {
            columns = Object.keys(row);
        }
        if (!stringifier) {
            stringifier = stringify({
                delimiter: ',',
                header: true,
                columns: columns
            });
            stringifier.pipe(process.stdout);
        }
        // console.log(row);
        stringifier.write(row);
    }, function(err, count) {
        // This is invoked when all rows are processed.
        if (err) { reject(err); }
        // console.log(`FINISHED ${count} rows`);
        stringifier.end();
        resolve();
    })
});

db.close(); 
