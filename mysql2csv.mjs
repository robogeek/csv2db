
import stringify from 'csv-stringify';
import mysql from 'mysql';

const dbhost  = process.argv[2];
const dbuser  = process.argv[3];
const dbpass  = process.argv[4];
const dbname  = process.argv[5];
const dbquery = process.argv[6];

const db = mysql.createConnection({
    host     : dbhost,
    user     : dbuser,
    password : dbpass,
    database : dbname,
    // debug    : true
});
db.on('error', err => {
    console.log(err);
    process.exit(-1);
});

await new Promise((resolve, reject) => {
    db.connect((err) => {
        if (err) {
            console.error('error connecting: ' + err.stack);
            reject(err);
        } else {
            resolve();
        }
    });
});

let stringifier;
let columns;
const query = db.query(dbquery);
query
  .on('error', function(err) {
    // Handle error, an 'end' event will be emitted after this as well
    console.log(err);
    process.exit(-1);
  })
  .on('fields', function(fields) {
    // the field packets for the rows to follow

    columns = fields.map(field => field.name);
    stringifier = stringify({
        delimiter: ',',
        header: true,
        columns: columns
    });
    stringifier.pipe(process.stdout);

  })
  .on('result', function(row) {
    db.pause();
    stringifier.write(row);
    db.resume();
  })
  .on('end', function() {
    // all rows have been received
    stringifier.end();
    db.end(); 
  });