Examples for interchanging CSV files with databases.  They handle importing data from CSV to either SQLite3 or MySQL, or exporting data as CSV from SQLite3 or MySQL.

For a writeup, see:  https://techsparx.com/nodejs/howto/csv2mysql.html

The scripts are written using ES6 modules, and use top-level async/await keywords for asynchronous operation.  This requires using Node.js 14.8 or later, as described here: https://techsparx.com/nodejs/async/top-level-async.html
