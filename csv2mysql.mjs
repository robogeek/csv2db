
import parse from 'csv-parse';
import fs from 'fs';
import mysql from 'mysql';
import csvHeaders from 'csv-headers';

const dbhost = process.argv[2];
const dbuser = process.argv[3];
const dbpass = process.argv[4];
const dbname = process.argv[5];
const tblnm  = process.argv[6];
const csvfn  = process.argv[7];

const headers = await new Promise((resolve, reject) => {
    csvHeaders({
        file      : csvfn,
        delimiter : ','
    }, function(err, headers) {
        if (err) reject(err);
        else {
            resolve(headers.map(hdr => {
                // Sometimes header names are :- "Long Header Name"
                // We need to remove the "-marks
                return hdr.replace(/["]/g, '');
            }));
        }
    });
});

const db = mysql.createConnection({
    host     : dbhost,
    user     : dbuser,
    password : dbpass,
    database : dbname,
    // debug    : true
});
db.on('error', err => { 
    console.log(err);
    process.exit(-1);
});

await new Promise((resolve, reject) => {
    db.connect((err) => {
        if (err) {
            console.error('error connecting: ' + err.stack);
            reject(err);
        } else {
            resolve();
        }
    });
});

await new Promise((resolve, reject) => {
    db.query(`DROP TABLE IF EXISTS ${tblnm}`,
    [ ],
    err => {
        if (err) reject(err);
        else resolve();
    })
});

// console.log(headers);

const fixfieldnm = (nm) => { return nm.replace(/[ "]/g, '_'); }

// Convert header names into names suitable for database schema
const fields = headers.map(hdr => {
    return fixfieldnm(hdr);
});

// console.log(fields);

// Convert fields array into one with TEXT data type
const schema = headers.map(field => {
    return `${fixfieldnm(field)} TEXT`;
});

// console.log(schema);
// console.log(schema.join(', '));

// Generate an array of question markes for use in prepared statements
const questionMarks = headers.map(field => {
    return '?';
});

// console.log(questionMarks);
// console.log(questionMarks.join(', '));

await new Promise((resolve, reject) => {
    // console.log(`about to create CREATE TABLE IF NOT EXISTS ${tblnm} ( ${fields} )`);
    db.query(`CREATE TABLE IF NOT EXISTS ${tblnm} ( ${schema.join(', ')} )`,
    [ ],
    err => {
        if (err) reject(err);
        else resolve();
    })
});

// Read the CSV file using Node.js streaming
const parser = fs.createReadStream(csvfn).pipe(parse({
    delimiter: ',',
    columns: true,
    relax_column_count: true
}));

// Receive each row of the CSV file, insert into database
for await (const row of parser) {

    // Possibly there is custom processing required,
    // For example you might know of certain fields, and
    // of certain constraints.  In this case we might know
    // that certain records are to be skipped.
    // if (row['Email'] === '(Not disclosed)') continue;

    // console.log(row);
    let d = [];
    headers.forEach(hdr => {
        // console.log(`${hdr} ${row[hdr.replace(/["]/g, '')]}`);
        d.push(row[hdr]);
    });
    await new Promise((resolve, reject) => {
        // console.log(`${fields.join(', ')}`, d);
        db.query(`INSERT INTO ${tblnm} ( ${fields.join(', ')} )
                VALUES ( ${questionMarks.join(', ')} )`, d,
        err => {
            if (err) {
                console.log(`${fields.join(', ')}`, d);
                console.error(err);
                /* reject(err); */ resolve();
            }
            else resolve();
        });
    });
}

db.end(); 
